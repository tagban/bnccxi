---------------------------------------------------------------------------------------------------
-- func: speed
-- desc: Resets player movement speed.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0,
    parameters = ""
};

function onTrigger(player, speed)
    player:speed(90);
end;