---------------------------------------------------------------------------------------------------
-- func: wallhack <optional target>
-- desc: Allows the player to walk through walls.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0,
    parameters = "s"
};

function error(player, msg)
    player:PrintToPlayer(msg);
    player:PrintToPlayer("!wallhack {player}");
end;

function onTrigger(player, target)
if (player:getVar( 'inJail' ) >= 1) then
 error(player, "You are in Jail. You can't do shit. Reach out to GM.")
return
end
    if (player:checkNameFlags(0x00000200)) then
        player:setFlag(0x00000200);
        player:PrintToPlayer( string.format("Toggled %s's wallhack flag OFF.", player:getName()) );
    else
        player:setFlag(0x00000200);
        player:PrintToPlayer( string.format("Toggled %s's wallhack flag ON.", player:getName()) );
    end
end
