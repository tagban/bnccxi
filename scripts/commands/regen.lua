---------------------------------------------------------------------------------------------------
-- func: Server wide buffs.
-- desc: gives players refresh/regen/protect/shell/haste.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0,
    parameters = "i"
};

function onTrigger(player) -- 
    player:addStatusEffect(dsp.effect.REFRESH,3,0,0);
    player:addStatusEffect(dsp.effect.HASTE,2,0,0);
	player:addStatusEffect(dsp.effect.PROTECT,3,0,0);
	player:addStatusEffect(dsp.effect.SHELL,3,0,0);
    player:addStatusEffect(dsp.effect.REGEN,2,0,0);
    if (player:getMainLvl() <= 60) then
        player:addStatusEffect(dsp.effect.REGEN,3,0,0);
    else
        player:addStatusEffect(dsp.effect.REGEN,2,0,0);
    end
end