---------------------------------------------------------------------------------------------------
-- func: costume
-- desc: Sets the players current costume.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0,
    parameters = "i"
};

function error(player, msg)
    player:PrintToPlayer(msg);
    player:PrintToPlayer("!costume <costumeID>");
end;

function onTrigger(player, costumeId)
if (player:getVar( 'inJail' ) >= 1) then
 error(player, "You are in Jail. You can't do shit. Reach out to GM.")
return
end
    -- validate costumeId
    if (costumeId == nil or costumeId < 0) then
        error(player, "Invalid costumeID.");
        return;
    end
    
    -- put on costume
    player:costume( costumeId );
end