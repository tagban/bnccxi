--------------------------------------------------------------
-- func: apocnigh
-- desc: opens a custom shop to access buying the apoc nigh earrings.
--------------------------------------------------------------

cmdprops =
{
    permission = 0,
    parameters = ""
};

require("scripts/globals/missions");
require("scripts/globals/titles");

function onTrigger(player)
if (player:getVar( 'inJail' ) >= 1) then
 error(player, "You are in Jail. You can't do shit. Reach out to GM.")
return
end

    -- Must have RoZ and CoP done and rank 9.
    local Zilart_clear = player:hasCompletedMission(ZILART,AWAKENING);
    local Chains_clear = player:hasCompletedMission(COP,DAWN);
    local current_rank = player:getRank();

    if (Zilart_clear == true and Chains_clear == true and current_rank >= 9) then

        local stock = { 
            15965, 500000, -- Ethereal Earring
            15964, 500000, -- Hallow Earring
            15963, 500000, -- Magnetic Earring
            15962, 500000, -- Static Earring
        };

        dsp.shop.general(player, stock);
        player:PrintToPlayer("You have proven your worth, enjoy the rewards..");

    else
        player:PrintToPlayer("You are not worthy..");
    end
end