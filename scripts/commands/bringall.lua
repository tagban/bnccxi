---------------------------------------------------------------------------------------------------
-- func: bringall
-- desc: Brings the users full alliance to them.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = ""
};

function onTrigger(player)
    local p = player:getAlliance();
    if (p ~= nil) then
        for _, member in pairs(p) do
            if (member:isPC() and member:getID() ~= player:getID()) then
                member:setPos( player:getXPos(), player:getYPos(), player:getZPos(), 0, player:getZoneID() );
            end
        end

    end
end